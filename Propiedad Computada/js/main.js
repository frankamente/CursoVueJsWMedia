const vm = new Vue({
	el: 'main',
	data: {
		mensaje: "Hola amigo",
		tareas: [{
				nombre: "Aprender c++",
				prioridad: true,
				antiguedad: 555
			},
			{
				nombre: "Hacer la comida",
				prioridad: true,
				antiguedad: 44
			},
			{
				nombre: "Leer Clean Code",
				prioridad: false,
				antiguedad: 2000
			}
		]
	},
	computed: {
		mensajeAlReves() {
			return this.mensaje.split('').reverse().join('');
		},
		tareasConPrioridad() {
			return this.tareas.filter((el) => el.prioridad);
		},
		tareasPorAntiguedad() {
			return this.tareas.sort((a, b) => b.antiguedad - a.antiguedad)
		}
	}
});