const vm = new Vue({
	el: 'main',
	data: {
		diasLaborales: ['Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes'],
		tareas: [{
				nombre: 'Hacer la comida',
				prioridad: 'Baja'
			},
			{
				nombre: 'Leer Clean Code',
				prioridad: 'Media'
			},
			{
				nombre: 'Aprender C++',
				prioridad: 'Alta'
			}
		],
		persona: {
			nombre: 'Fran',
			profesión: 'desarrollador',
			ciudad: 'Ronda'
		}
	}
});