const vm = new Vue({
	el: 'main',
	data: {
		nuevaTarea: '',
		tareas: [
			'Hacer la comida',
			'Leer Clean Code',
			'Aprender C++'
		],
	},
	methods: {
		agregarTarea() {
			// this, hace referencia a la instancia Vue
			this.tareas.unshift(this.nuevaTarea);
			this.nuevaTarea = '';
		}
	}
});

// Vanilla Javascript
// function agregarTarea() {
// 	const input = document.querySelector('input[type=text]');
// 	vm.tareas.unshift(input.value);
// 	input.value = '';
// }