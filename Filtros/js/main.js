Vue.filter('alReves', (valor) =>
	valor.split('').reverse().join('')
);

const vm = new Vue({
	el: 'main',
	data: {
		busqueda: "",
		minimo: 5,
		juegos: [{
				titulo: "Call of Duty",
				genero: "FPS",
				puntuacion: 9
			},
			{
				titulo: "Age of Empire",
				genero: "Estrategia",
				puntuacion: 7
			},
			{
				titulo: "Final Fantasy",
				genero: "Rpg",
				puntuacion: 4
			}
		],
	},
	computed: {
		mejoresJuegos() {
			return this.juegos.filter(juego => juego.puntuacion > this.minimo);
		},
		buscarJuegos() {
			return this.juegos.filter(juego => juego.titulo.includes(this.busqueda))
		}
	}
});